const _dobra = ({ param, setState }) => setState(state => ({ ...state, count: state.count * param }));
const _decrement = ({ setState }) => setState(state => ({ ...state, count: state.count - 1 }));
const _increment = ({ setState }) => setState(state => ({ ...state, count: state.count + 1 }));
const _counterInitial = {
	count: 0,
};

export { _dobra, _decrement, _increment, _counterInitial };
