import React, { useState, useMemo, useContext } from 'react';
import { _decrement, _dobra, _increment, _counterInitial } from './Counter';

const StoreContext = React.createContext({});

const _INITIALSTATE_ = { ..._counterInitial };

const useStoreState = () => {
	// Manage the state using React.useState()
	const [state, setState] = useState(_INITIALSTATE_);

	// Build our actions. We'll use useMemo() as an optimization,
	// so this will only ever be called once.
	const actions = useMemo(() => getActions(setState), [setState]);

	return { state, actions };
};

const getActions = setState => ({
	increment: () => _increment({ setState }),
	decrement: () => _decrement({ setState }),
	dobra: param => _dobra({ param, setState }),
});

const useStoreContext = () => useContext(StoreContext);

export { useStoreContext, StoreContext, useStoreState };
