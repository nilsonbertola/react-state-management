import React from 'react';
import logo from './logo.svg';
import './App.css';
import MyApp from './app/MyApp';
import { StoreContext, useStoreState } from './store/Store';

function App() {
	const { state, actions } = useStoreState();
	return (
		<StoreContext.Provider value={{ state, actions }}>
			<div className="App">
				<MyApp />
			</div>
		</StoreContext.Provider>
	);
}

export default App;
