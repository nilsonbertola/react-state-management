import React from 'react';
import Increment from './Increment';
import Decrement from './Decrement';
import { useStoreContext } from '../store/Store';
import Dobra from './Dobra';

const MyApp = () => {
	const { state } = useStoreContext();
	return (
		<div>
			<Decrement />
			<span>{state.count}</span>
			<Increment />
			<Dobra />
		</div>
	);
};
export default MyApp;
