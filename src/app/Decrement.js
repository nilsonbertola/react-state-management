import React from 'react';
import { useStoreContext } from '../store/Store';

const Decrement = () => {
	const { actions } = useStoreContext();
	return <button onClick={actions.decrement}>-</button>;
};

export default Decrement;
