import React from 'react';
import { useStoreContext } from '../store/Store';

const Increment = () => {
	const { actions } = useStoreContext();
	return <button onClick={actions.increment}>+</button>;
};

export default Increment;
