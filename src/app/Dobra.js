import React from 'react';
import { useStoreContext } from '../store/Store';

const Dobra = () => {
	const { actions } = useStoreContext();
	return <button onClick={() => actions.dobra(2)}>x2</button>;
};

export default Dobra;
